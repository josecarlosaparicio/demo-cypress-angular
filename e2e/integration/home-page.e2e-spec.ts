/// <reference types="cypress" />

describe('Home Page', () => {
  it('should display the app name on the home page', () => {
    cy.visit('/');

    cy.contains('Angular Material').click();
    cy.get('.terminal').should('include.text', 'ng add @angular/material');
  });
});
